import urllib2
import re
import json

def _get_video_links_json(video_id):
	url = "http://www.dailymotion.com/embed/video/" + video_id
	header = {'Cookie' : 'lang=en_EN; family_filter=on'}
	req = urllib2.Request(url, None, header)
	try:
		response = urllib2.urlopen(req)
	except urllib2.URLError as e:
		if hasattr(e, 'reason'):
			raise RuntimeError(str(e.reason))
		elif hasattr(e, 'code'):
			raise RuntimeError(str(e.code))
	else:
		response_data = response.read()
		if '"statusCode":' in response_data:
			raise RuntimeError('Video not found')
			return ""
		video_info =  re.compile(r'dmp\.create\(document\.getElementById\(\'player\'\),\s*([^;]+)').findall(response_data)[0]
		video_info = video_info[:len(video_info)-1]
		return json.loads(video_info)['metadata']['qualities']


def get_quality_video_link(quality, url):
	video_id = re.compile(r'https?://(?:www.)?dailymotion.com/video/([\w-]+)').findall(url)[0]
	video_json = _get_video_links_json(video_id)

	if '1080' in video_json.keys():
		return video_json['1080'][0]['url']
	elif '720' in video_json.keys():
		return video_json['720'][0]['url']
	elif '480' in video_json.keys():
		return video_json['480'][0]['url']
	elif '380' in video_json.keys():
		return video_json['380'][0]['url']
	elif '240' in video_json.keys():
		return video_json['240'][0]['url']
	elif 'auto' in video_json.keys():
		return video_json['auto'][0]['url']
	else:
		return ''
