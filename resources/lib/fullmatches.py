#-*- coding: utf-8 -*-
# fullmatchesandshows

import urllib
import urllib2
from urlparse import urlparse
import re
import time


def _get_page_source(url,data=None,header=None):
	req = urllib2.Request(url, data, header)
	try:
		response = urllib2.urlopen(req)
	except urllib2.URLError as e:
		if hasattr(e, 'reason'):
			raise RuntimeError(str(e.reason))
		elif hasattr(e, 'code'):
			raise RuntimeError(str(e.code))
	else:
		return response.read()

def get_quality_video_link(url,data=None,header=None):
	videourl=[]
	response_data = _get_page_source(url,data,header)

	fullheader = header
	fullheader['X-Requested-With']='XMLHttpRequest'

	''' multiple videos listed on the page '''
	items = re.compile(r'<div class="acp_title">(\w+\s\w+(\s\w+)?)</div>').findall(response_data)

	if len(items) > 1:
		for pos, item in enumerate(items, start=1):
			try:
				next_url = nexturl(url,pos)
				response_data = _get_page_source(next_url, data, header)
				videourl.append({'url':parse(response_data,header), 'title':item[0]})
			except:
				continue
	else:
		title = re.compile(r'<title>(.*)</title>').findall(response_data)[0]
		videourl.append({'url':parse(response_data, header), 'title':title})
	return videourl

def nexturl(url,pos):
	if urlparse(url).path.count('/') == 5:
		# we are the base url, append 1/ to the url
		# http://www.fullmatchesandshows.com/year/month/day/match/1/
		return url+str(pos)+'/'
	elif urlparse(url).path.count('/') == 6:
		# remove N/ and replace it with the pos
		return url[:-2]+str(pos)+'/'

def parse(res, header=None):
	ret=""
	video_info = re.compile(r'config.playwire.com/(.+?)/videos/v2/(.+?)/zeus.json').findall(res)
        if len(video_info) > 1:
		ret='http://cdn.phoenix.intergi.com/' + video_info[0] + '/videos/' + video_info[1] + '/video-sd.mp4?hosting_id=' + video_info[0]
	else:
		streamable_url = re.compile(r'data-lazy-src="\/\/(streamable\.com\/s\/\w+\/\w+)"').findall(res)
		streamable_res = _get_page_source('https://{}'.format(streamable_url[0]), None, header)
		ret = 'https://{}'.format(re.compile(r'"url":\s+"\/\/(.*?)"').findall(streamable_res)[0])
	return ret
