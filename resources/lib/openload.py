#-*- coding: utf-8 -*-

import urllib
import urllib2
import re
import time
import os

def _get_page_source(url,data=None,header=None):
	req = urllib2.Request(url, data, header)
	try:
		response = urllib2.urlopen(req)
	except urllib2.URLError as e:
		if hasattr(e, 'reason'):
			raise RuntimeError(str(e.reason))
		elif hasattr(e, 'code'):
			raise RuntimeError(str(e.code))
	else:
		return response.read()

def get_quality_video_link(url,data=None,header=None):
	openloadurl = re.compile(r"//(?:www\.)?openload\.(?:co|io)?/(?:embed|f)/([0-9a-zA-Z-_]+)", re.DOTALL | re.IGNORECASE).findall(url)[0]
	openloadurl = 'https://openload.co/embed/%s/' % openloadurl
	response_data = _get_page_source(openloadurl,data,header)

	aastring = re.search(r"<video(?:.|\s)*?<script\s[^>]*?>((?:.|\s)*?)</script", response_data, re.DOTALL | re.IGNORECASE).group(1)

	aastring = aastring.replace("((ﾟｰﾟ) + (ﾟｰﾟ) + (ﾟΘﾟ))", "9")
	aastring = aastring.replace("((ﾟｰﾟ) + (ﾟｰﾟ))","8")
	aastring = aastring.replace("((ﾟｰﾟ) + (o^_^o))","7")
	aastring = aastring.replace("((o^_^o) +(o^_^o))","6")
	aastring = aastring.replace("((ﾟｰﾟ) + (ﾟΘﾟ))","5")
	aastring = aastring.replace("(ﾟｰﾟ)","4")
	aastring = aastring.replace("((o^_^o) - (ﾟΘﾟ))","2")
	aastring = aastring.replace("(o^_^o)","3")
	aastring = aastring.replace("(ﾟΘﾟ)","1")
	aastring = aastring.replace("(c^_^o)","0")
	aastring = aastring.replace("(ﾟДﾟ)[ﾟεﾟ]","\\")
	aastring = aastring.replace("(3 +3 +0)","6")
	aastring = aastring.replace("(3 - 1 +0)","2")
	aastring = aastring.replace("(1 -0)","1")
	aastring = aastring.replace("(4 -0)","4")
	
	decodestring = re.search(r"\\\+([^(]+)", aastring, re.DOTALL | re.IGNORECASE).group(1)
	decodestring = "\\+"+ decodestring
	decodestring = decodestring.replace("+","")
	decodestring = decodestring.replace(" ","")
    
	decodestring = decode(decodestring)
	decodestring = decodestring.replace("\\/","/")
    
	videourl = re.search(r'window.vr=\"(.*)\";window', decodestring, re.DOTALL | re.IGNORECASE).group(1)
	return videourl

def decode(encoded):
    for octc in (c for c in re.findall(r'\\(\d{2,3})', encoded)):
        encoded = encoded.replace(r'\%s' % octc, chr(int(octc, 8)))
    return encoded.decode('utf8')
