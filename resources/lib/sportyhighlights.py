# sporthightlights.com
# hosts a collection of other content
# youtube, playwire and vid.me


import urllib2
import re

def _get_page_source(url):
	req = urllib2.Request(url)
	try:
		response = urllib2.urlopen(req)
	except urllib2.URLError as e:
		if hasattr(e, 'reason'):
			raise RuntimeError(str(e.reason))
		elif hasattr(e, 'code'):
			raise RuntimeError(str(e.code))
	else:
		return response.read()

def get_quality_video_link(url):
	response_data = _get_page_source(url)

	if re.search('youtube', response_data, re.IGNORECASE):
		key = re.compile(r'https://www.youtube.com/embed/([\w-]+).*').findall(response_data)[0]
		return "https://www.youtube.com/watch?v=%s" % (key)
	else:
		pass